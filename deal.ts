import { initializeDominos, shuffle, Domino} from "./dominos";

export interface Hands {
    playerNorth: Domino[];
    playerEast:  Domino[];
    playerSouth: Domino[];
    playerWest:  Domino[];
}

export function dealHands(firstPLayer: string): Hands {
    const newSet = initializeDominos();
    const shuffledSet = shuffle(newSet);

    let north: Domino[] = [];
    let east: Domino[] = [];
    let south: Domino[] = [];
    let west: Domino[] = [];

    switch (firstPLayer) {
        case 'north':
            
    }
    
    return {
      playerNorth: north,
      playerEast: east,
      playerSouth: south,
      playerWest: west  
    };
}
