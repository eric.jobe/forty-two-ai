export interface Domino {
    name: string;
    left: number;
    right: number;
}

export function initializeDominos(): Domino[] {
    let dominoSet: Domino[] = [];

    for(let i = 6; i >=0; i--) {
        for(let j = i; j >= 0; j--) {
            dominoSet.push({
                name: `[${i}:${j}]`,
                left: i,
                right: j
            });
        }
    }
    return dominoSet;
}

export function getSuit(suitNumber: number): Domino[] {
    let suit: Domino[] = [];
    
    // Push double
    suit.push({
        name: `[${suitNumber}:${suitNumber}]`,
        left: suitNumber,
        right: suitNumber 
    });

    // Push rest of suit minus double
    for(let i = 6; i >= 0; i--) {
        if(i !== suitNumber) {
            suit.push({
                name: `[${suitNumber}:${i}]`,
                left: suitNumber,
                right: i
            });
        }
    }
    
    return suit;
}

export function shuffle(set: Domino[]): Domino[] {
    for (let i = set.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [set[i], set[j]] = [set[j], set[i]];
    }

    return set;
}